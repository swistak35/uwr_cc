#include "assert.h"
#include "reader.h"
#include "tokenizer.h"
#include "varstore.h"
#include "parser.h"
#include <memory>

#include "ast/arg.h"
#include "ast/expr.h"
#include "ast/double_expr.h"
#include "ast/int_expr.h"
#include "ast/variable_expr.h"
#include "ast/binary_expr.h"
#include "ast/assignment_expr.h"
#include "ast/call_expr.h"
#include "ast/while_expr.h"
#include "ast/if_expr.h"
#include "ast/return_expr.h"
#include "ast/top_level_definition.h"
#include "ast/extern_definition.h"
#include "ast/function_definition.h"

// for IR compiler
#include "llvm/ADT/STLExtras.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Verifier.h"

using namespace llvm;

/*
 * Functions transforming `tree` structs to our AST
 */

std::unique_ptr<ExprAST> transform(tree t) {
   const std::string nodeName = dynamic_cast<const stringvalue*>(t.pntr->val)->s;

   if (nodeName.compare("ASSIGN") == 0) {
      const std::string name = dynamic_cast<const stringvalue *>(t.pntr->subtrees.at(0).pntr->val)->s;
      std::unique_ptr<ExprAST> expr = transform(t.pntr->subtrees.at(1));
      return std::unique_ptr<ExprAST>(new AssignmentExprAST(name, std::move(expr)));
   } else if (nodeName.compare("OPCALL") == 0) {
      const std::string op = dynamic_cast<const stringvalue *>(t.pntr->subtrees.at(0).pntr->val)->s;
      std::unique_ptr<ExprAST> lhs = transform(t.pntr->subtrees.at(1));
      std::unique_ptr<ExprAST> rhs = transform(t.pntr->subtrees.at(2));
      return std::unique_ptr<ExprAST>(new BinaryExprAST(op, std::move(lhs), std::move(rhs)));
   } else if (nodeName.compare("INT") == 0) {
      int val = dynamic_cast<const doublevalue *>(t.pntr->subtrees.at(0).pntr->val)->d;
      return std::unique_ptr<ExprAST>(new IntExprAST(val));
   } else if (nodeName.compare("VAR") == 0) {
      const std::string name = dynamic_cast<const stringvalue *>(t.pntr->subtrees.at(0).pntr->val)->s;
      return std::unique_ptr<ExprAST>(new VariableExprAST(name));
   } else if (nodeName.compare("FUNCALL") == 0) {
      const std::string name = dynamic_cast<const stringvalue *>(t.pntr->subtrees.at(0).pntr->val)->s;
      std::vector< std::unique_ptr<ExprAST> > args;
      for (size_t i = 1; i < t.pntr->subtrees.size(); i++) {
         args.push_back(transform(t.pntr->subtrees.at(i)));
      }
      return std::unique_ptr<ExprAST>(new CallExprAST(name, std::move(args)));
   } else if (nodeName.compare("WHILE") == 0) {
      std::unique_ptr<ExprAST> condition = transform(t.pntr->subtrees.at(0));
      std::vector< std::unique_ptr<ExprAST> > body;
      for (auto &a : t.pntr->subtrees.at(1).pntr->subtrees) {
         body.push_back(transform(a));
      }
      return std::unique_ptr<ExprAST>(new WhileExprAST(std::move(condition), std::move(body)));
   } else if (nodeName.compare("EXPRESSION") == 0) {
      std::unique_ptr<ExprAST> e = transform(t.pntr->subtrees.at(0));
      return e;
   } else if (nodeName.compare("RETURN") == 0) {
      std::unique_ptr<ExprAST> result = transform(t.pntr->subtrees.at(0));
      return std::unique_ptr<ExprAST>(new ReturnExprAST(std::move(result)));
   } else if (nodeName.compare("IF") == 0) {
      std::unique_ptr<ExprAST> condition = transform(t.pntr->subtrees.at(0));
      std::vector< std::unique_ptr<ExprAST> > ifTrue;
      for (auto &a : t.pntr->subtrees.at(1).pntr->subtrees) {
         ifTrue.push_back(transform(a));
      }
      std::vector< std::unique_ptr<ExprAST> > ifFalse;
      for (auto &a : t.pntr->subtrees.at(2).pntr->subtrees) {
         ifFalse.push_back(transform(a));
      }
      return std::unique_ptr<ExprAST>(new IfExprAST(std::move(condition), std::move(ifTrue), std::move(ifFalse)));
   }
   std::cout << "ExprAST name: " << nodeName << std::endl;
   return std::unique_ptr<ExprAST>(new IntExprAST(3));
}

std::unique_ptr<TopLevelDefinitionAST> transform_func(tree t) {
   const std::string nodeName = dynamic_cast<const stringvalue*>(t.pntr->val)->s;

   if (nodeName.compare("EXTERN") == 0) {
      const std::string returnType = dynamic_cast<const stringvalue *>(t.pntr->subtrees.at(0).pntr->val)->s;
      const std::string name = dynamic_cast<const stringvalue *>(t.pntr->subtrees.at(1).pntr->val)->s;
      std::vector< std::unique_ptr<ArgAST> > args;
      for (auto &a : t.pntr->subtrees.at(2).pntr->subtrees) {
         const std::string argType = dynamic_cast<const stringvalue *>(a.pntr->subtrees.at(0).pntr->val)->s;
         const std::string argName = dynamic_cast<const stringvalue *>(a.pntr->subtrees.at(1).pntr->val)->s;
         args.push_back(std::unique_ptr<ArgAST>(new ArgAST(argName, argType)));
      }
      return std::unique_ptr<ExternDefinitionAST>(new ExternDefinitionAST(name, returnType, std::move(args)));
   } else if (nodeName.compare("FUN") == 0) {
      const std::string returnType = dynamic_cast<const stringvalue *>(t.pntr->subtrees.at(0).pntr->val)->s;
      const std::string name = dynamic_cast<const stringvalue *>(t.pntr->subtrees.at(1).pntr->val)->s;
      std::vector< std::unique_ptr<ArgAST> > args;
      for (auto &a : t.pntr->subtrees.at(2).pntr->subtrees) {
         const std::string argType = dynamic_cast<const stringvalue *>(a.pntr->subtrees.at(0).pntr->val)->s;
         const std::string argName = dynamic_cast<const stringvalue *>(a.pntr->subtrees.at(1).pntr->val)->s;
         args.push_back(std::unique_ptr<ArgAST>(new ArgAST(argName, argType)));
      }
      std::vector< std::unique_ptr<ExprAST> > body;
      for (auto &a : t.pntr->subtrees.at(3).pntr->subtrees) {
         body.push_back(transform(a));
      }
      return std::unique_ptr<FunctionDefinitionAST>(new FunctionDefinitionAST(name, returnType, std::move(args), std::move(body)));
   }
}

/*
 * IR Compiler
 */

Value *ErrorV(const char *str) {
  fprintf(stderr, "Error: %s\n", str);
  return 0;
}

static std::unique_ptr<Module> TheModule;
static IRBuilder<> Builder(getGlobalContext());
static std::map<std::string, AllocaInst*> NamedValues;

/// CreateEntryBlockAlloca - Create an alloca instruction in the entry block of
/// the function.  This is used for mutable variables etc.
static AllocaInst *CreateEntryBlockAlloca(Function *TheFunction, const std::string &VarName) {
  IRBuilder<> TmpB(&TheFunction->getEntryBlock(), TheFunction->getEntryBlock().begin());
  return TmpB.CreateAlloca(Type::getInt32Ty(getGlobalContext()), 0, VarName.c_str());
}

Value * DoubleExprAST::codegen() {
  return ConstantFP::get(getGlobalContext(), APFloat(Val));
}

Value * IntExprAST::codegen() {
  return ConstantInt::get(getGlobalContext(), APInt(32, Val));
}

Value *VariableExprAST::codegen() {
  // Look this variable up in the function.
  Value *V = NamedValues[Name];

  if (V == 0) return ErrorV("Unknown variable name");

  // Load the value.
  return Builder.CreateLoad(V, Name.c_str());
}

Value *ReturnExprAST::codegen() {
  // Look this variable up in the function.
  return Result->codegen();
}

Value *BinaryExprAST::codegen() {
  Value *L = LHS->codegen();
  Value *R = RHS->codegen();

  if (L == 0 || R == 0) {
    return 0;
  }

  if (Op.compare("+") == 0) {
    return Builder.CreateAdd(L, R, "addtmp");
  } else if (Op.compare("*") == 0) {
    return Builder.CreateMul(L, R, "multmp");
  } else if (Op.compare("-") == 0) {
    return Builder.CreateSub(L, R, "subtmp");
  } else if (Op.compare("==") == 0) {
    return Builder.CreateICmpEQ(L, R, "cmptmp");
  } else if (Op.compare(">=") == 0) {
    return Builder.CreateICmpSGE(L, R, "cmptmp");
  } else if (Op.compare("<=") == 0) {
    return Builder.CreateICmpSLE(L, R, "cmptmp");
  } else if (Op.compare(">") == 0) {
    return Builder.CreateICmpSGT(L, R, "cmptmp");
  } else if (Op.compare("<") == 0) {
    return Builder.CreateICmpSLT(L, R, "cmptmp");
  } else if (Op.compare("!=") == 0) {
    return Builder.CreateICmpNE(L, R, "cmptmp");
  } else {
    return ErrorV("invalid binary operator");
  }
}

Value * CallExprAST::codegen() {
  // Look up the name in the global module table.
  Function *CalleeF = TheModule->getFunction(Callee);
  if (CalleeF == 0)
    return ErrorV("Unknown function referenced");

  // If argument mismatch error.
  if (CalleeF->arg_size() != Args.size())
    return ErrorV("Incorrect # arguments passed");

  std::vector<Value*> ArgsV;
  for (unsigned i = 0, e = Args.size(); i != e; ++i) {
    ArgsV.push_back(Args[i]->codegen());
    if (ArgsV.back() == 0) return 0;
  }

  return Builder.CreateCall(CalleeF, ArgsV, "calltmp");
}

Value * IfExprAST::codegen() {
  Value *CondV = Condition->codegen();

  if (CondV == 0) {
    // generating of condition code failed
    return 0;
  }

  // Convert condition to a bool by comparing equal to 0.0.
  CondV = Builder.CreateICmpNE(CondV,
      ConstantInt::get(getGlobalContext(), APInt(1, 0)),
      "ifcond");

  Function *TheFunction = Builder.GetInsertBlock()->getParent();

  // Create blocks for the then and else cases.  Insert the 'then' block at the
  // end of the function.
  BasicBlock *ThenBB = BasicBlock::Create(getGlobalContext(), "then", TheFunction);
  BasicBlock *ElseBB = BasicBlock::Create(getGlobalContext(), "else");
  BasicBlock *MergeBB = BasicBlock::Create(getGlobalContext(), "ifcont");

  Builder.CreateCondBr(CondV, ThenBB, ElseBB);

  // Emit then value.
  Builder.SetInsertPoint(ThenBB);

  Value *ThenV;
  for (auto &a : IfTrue) {
    ThenV = a->codegen();
  }
  if (ThenV == 0) return 0;

  Builder.CreateBr(MergeBB);
  // Codegen of 'Then' can change the current block, update ThenBB for the PHI.
  ThenBB = Builder.GetInsertBlock();

  // Emit else block.
  TheFunction->getBasicBlockList().push_back(ElseBB);
  Builder.SetInsertPoint(ElseBB);

  Value *ElseV;
  for (auto &a : IfFalse) {
    ElseV = a->codegen();
  }
  if (ElseV == 0) return 0;

  Builder.CreateBr(MergeBB);
  // Codegen of 'Else' can change the current block, update ElseBB for the PHI.
  ElseBB = Builder.GetInsertBlock();

  // Emit merge block.
  TheFunction->getBasicBlockList().push_back(MergeBB);
  Builder.SetInsertPoint(MergeBB);
  PHINode *PN = Builder.CreatePHI(Type::getInt32Ty(getGlobalContext()), 2, "iftmp");

  PN->addIncoming(ThenV, ThenBB);
  PN->addIncoming(ElseV, ElseBB);
  return PN;
}

Value * WhileExprAST::codegen() {
  Function *TheFunction = Builder.GetInsertBlock()->getParent();
  BasicBlock *BeforeBB = BasicBlock::Create(getGlobalContext(), "beforeloop", TheFunction);
  BasicBlock *LoopBB = BasicBlock::Create(getGlobalContext(), "loop", TheFunction);
  BasicBlock *AfterBB = BasicBlock::Create(getGlobalContext(), "afterloop", TheFunction);

  // Jump from current place to the block in which we will check While condition
  Builder.CreateBr(BeforeBB);

  // Start insertion in block when we will check While condition
  Builder.SetInsertPoint(BeforeBB);

  // Generate code for While condition
  Value * endCondition = Condition->codegen();
  if (endCondition == 0) return 0;

  endCondition = Builder.CreateICmpNE(endCondition,
      ConstantInt::get(getGlobalContext(), APInt(1, 0)), "loopcnd");

  // Insert the conditional branch depending whether While condition is true or false
  Builder.CreateCondBr(endCondition, LoopBB, AfterBB);

  // Start insertion in LoopBB
  Builder.SetInsertPoint(LoopBB);
  
  // TODO: possible bug - what happens when while block empty?
  Value * BodyRes;
  for (auto &a : Body) {
    a->codegen();
  }
  if (BodyRes == 0) return 0;

  // After making an iteration of the loop, we want always to jump to block checking the condition
  Builder.CreateBr(BeforeBB);

  // Go back to the previous code, which should be generated before we started our while
  Builder.SetInsertPoint(AfterBB);

  // meh, return always 0
  return ConstantInt::get(getGlobalContext(), APInt(32, 0));
}

Function *FunctionDefinitionAST::codegen() {
  // First, check for an existing function from a previous 'extern' declaration.
  Function *TheFunction = TheModule->getFunction(Name);

  if (!TheFunction) {
    /* BEGIN TheFunction = Proto->codegen(); */
    // Make the function type:  double(double,double) etc.
    std::vector<Type *> Ints(Args.size(),
        Type::getInt32Ty(getGlobalContext()));
    FunctionType *FT =
      FunctionType::get(Type::getInt32Ty(getGlobalContext()), Ints, false);

    Function *F =
      Function::Create(FT, Function::ExternalLinkage, Name, TheModule.get());

    // Set names for all arguments.
    unsigned Idx = 0;
    for (auto &Arg : F->args()) {
      Arg.setName(Args[Idx++]->getName());
    }

    TheFunction = F;
    /* END TheFunction = Proto->codegen(); */
  }

  if (!TheFunction)
    return nullptr;

  // Create a new basic block to start insertion into.
  BasicBlock *BB = BasicBlock::Create(getGlobalContext(), "entry", TheFunction);
  Builder.SetInsertPoint(BB);

  // Create argument allocas
  {
    Function::arg_iterator AI = TheFunction->arg_begin();
    for (unsigned Idx = 0, e = Args.size(); Idx != e; ++Idx, ++AI) {
      // Create an alloca for this variable.
      AllocaInst *Alloca = CreateEntryBlockAlloca(TheFunction, Args[Idx]->getName());

      // Store the initial value into the alloca.
      Builder.CreateStore(AI, Alloca);

      // Add arguments to variable symbol table.
      NamedValues[Args[Idx]->getName()] = Alloca;
    }
  }

  Value * RetVal;
  for (auto &statement : Body) {
    RetVal = statement->codegen();
  }
  if (RetVal) {
    // Finish off the function.
    Builder.CreateRet(RetVal);

    // Validate the generated code, checking for consistency.
    verifyFunction(*TheFunction);

    return TheFunction;
  }

  // Error reading body, remove function.
  TheFunction->eraseFromParent();
  return nullptr;
}

Function *ExternDefinitionAST::codegen() {
  /* TheFunction = Proto->codegen(); */
  /*    // Make the function type:  double(double,double) etc. */
  std::vector<Type *> Ints(Args.size(),
      Type::getInt32Ty(getGlobalContext()));
  FunctionType *FT =
    FunctionType::get(Type::getInt32Ty(getGlobalContext()), Ints, false);

  Function *F =
    Function::Create(FT, Function::ExternalLinkage, Name, TheModule.get());

  // Set names for all arguments.
  unsigned Idx = 0;
  for (auto &Arg : F->args()) {
    Arg.setName(Args[Idx++]->getName());
  }

  return F;
}

Value * AssignmentExprAST::codegen() {
  Value *Val = Expr->codegen();
  if (Val == 0) return 0;

  // Look up the name.
  Value *Variable = NamedValues[Name];
  if (Variable == 0) return ErrorV("Unknown variable name");

  Builder.CreateStore(Val, Variable);
  // TODO: It could return val. meh, it could even be an operator, like it's in Kaleidoscope - no reason to not do it that way
  /* return Val; */
  return ConstantInt::get(getGlobalContext(), APInt(32, 0));
}

int main(int argc, char* argv []) {

  tokenizer tt;

  varstore vs;

  parser(tt, vs, tkn_Session, 15);

  ASSERT(tt.lookahead.size());

  if (tt.lookahead.front().type != tkn_Session) {
    std::cout << "ended with an error\n";
    return 1;
  }

  std::vector< std::unique_ptr<TopLevelDefinitionAST> > defs;
  for (auto a : tt.lookahead.front().tree) {
    /* std::cout << a << std::endl; */
    std::unique_ptr<TopLevelDefinitionAST> res = transform_func(a);
    /* res.get()->pretty_print(); */
    defs.push_back(std::move(res));
  }

  TheModule = llvm::make_unique<Module>("my cool jit", getGlobalContext());

  for (auto &func : defs) {
    func->codegen();
    /* Function *llvm_func = func->codegen(); */
    /* llvm_func->dump(); */
  }

  TheModule->dump();

  return 0;
}
