#include "llvm/IR/IRBuilder.h"

using namespace llvm;

#ifndef AST_EXPR

#define AST_EXPR

class ExprAST {
  public:
    virtual ~ExprAST() {}
    virtual void pretty_print() = 0;
    virtual Value * codegen() = 0;
};

#endif
