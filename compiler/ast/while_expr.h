#include <iostream>
#include "expr.h"

class WhileExprAST : public ExprAST {
  std::unique_ptr<ExprAST> Condition;
  std::vector<std::unique_ptr<ExprAST>> Body;

  public:
  WhileExprAST(std::unique_ptr<ExprAST> Condition,
      std::vector<std::unique_ptr<ExprAST>> Body)
    : Condition(std::move(Condition)), Body(std::move(Body)) {}

  void pretty_print();
  Value * codegen();
};
