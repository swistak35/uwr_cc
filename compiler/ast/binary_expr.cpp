#include "binary_expr.h"

void BinaryExprAST::pretty_print() {
  std::cout << "(";
  getLHS()->pretty_print();
  std::cout << " " << getOp() << " ";
  getRHS()->pretty_print();
  std::cout << ")";
}
