#include "function_definition.h"

void FunctionDefinitionAST::pretty_print() {
  std::cout << "fun " << ReturnType << " " << Name << "(";
  for (auto &a : Args) {
    a->pretty_print();
    std::cout << ", ";
  }
  std::cout << ") {\n";
  for (auto &a : Body) {
    a->pretty_print();
    std::cout << ";\n";
  }
  std::cout << "}\n";
}
