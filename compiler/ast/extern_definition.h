#include <iostream>
#include "arg.h"
#include "top_level_definition.h"

class ExternDefinitionAST : public TopLevelDefinitionAST {
  std::string Name;
  std::string ReturnType;
  std::vector<std::unique_ptr<ArgAST>> Args;

  public:
  ExternDefinitionAST(const std::string &Name,
      const std::string &ReturnType,
      std::vector<std::unique_ptr<ArgAST>> Args)
    : Name(Name), ReturnType(ReturnType), Args(std::move(Args)) {}

  void pretty_print();
  Function * codegen();
};
