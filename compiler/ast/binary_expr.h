#include <iostream>
#include "expr.h"

class BinaryExprAST : public ExprAST {
  std::string Op;
  std::unique_ptr<ExprAST> LHS, RHS;

  public:
    BinaryExprAST(const std::string &Op,
        std::unique_ptr<ExprAST> lhs,
        std::unique_ptr<ExprAST> rhs)
      : Op(Op), LHS(std::move(lhs)), RHS(std::move(rhs)) {}

    const std::string getOp() { return Op; }
    ExprAST * getLHS() { return LHS.get(); }
    ExprAST * getRHS() { return RHS.get(); }

    void pretty_print();
    Value * codegen();
};
