#include "call_expr.h"

void CallExprAST::pretty_print() {
  std::cout << getCallee() << "(";
  for (auto &a : getArgs()) {
    a->pretty_print();
    std::cout << ", ";
  }
  std::cout << ")";
}
