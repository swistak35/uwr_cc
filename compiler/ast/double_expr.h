#include <iostream>
#include "expr.h"

class DoubleExprAST : public ExprAST {
  double Val;

  public:
    DoubleExprAST(double Val) : Val(Val) {}

    double getVal() { return this->Val; }

    void pretty_print();
    Value * codegen();
};
