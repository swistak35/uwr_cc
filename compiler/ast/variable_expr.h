#include <iostream>
#include "expr.h"

class VariableExprAST : public ExprAST {
  std::string Name;

  public:
    VariableExprAST(const std::string &Name) : Name(Name) {}

    std::string getName() { return this->Name; }

    void pretty_print();
    Value * codegen();
};
