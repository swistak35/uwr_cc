#include <iostream>
#include "expr.h"

class AssignmentExprAST : public ExprAST {
  std::string Name;
  std::unique_ptr<ExprAST> Expr;

  public:
  AssignmentExprAST(const std::string &Name,
      std::unique_ptr<ExprAST> Expr)
    : Name(Name), Expr(std::move(Expr)) {}

  void pretty_print();
  Value * codegen();
};
