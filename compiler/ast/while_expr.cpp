#include "while_expr.h"

void WhileExprAST::pretty_print() {
  std::cout << "while (";
  Condition->pretty_print();
  std::cout << ") {\n";
  for (auto &a : Body) {
    a->pretty_print();
    std::cout << ";\n";
  }
  std::cout << "}";
}
