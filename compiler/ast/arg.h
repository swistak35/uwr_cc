#include <string>
#include <iostream>

#ifndef AST_ARG

#define AST_ARG

class ArgAST {
  std::string Name;
  std::string Type;

  public:
  ArgAST(const std::string &Name, const std::string &Type) : Name(Name), Type(Type) {}

  std::string getName() { return Name; }
  std::string getType() { return Type; }

  void pretty_print();
};

#endif
