#include "assignment_expr.h"

void AssignmentExprAST::pretty_print() {
  std::cout << Name << " := ";
  Expr->pretty_print();
}
