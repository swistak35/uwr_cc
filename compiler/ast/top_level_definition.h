#include "llvm/IR/IRBuilder.h"

using namespace llvm;

#ifndef AST_TOP_LEVEL_DEFINITION

#define AST_TOP_LEVEL_DEFINITION

class TopLevelDefinitionAST {
  public:
    virtual ~TopLevelDefinitionAST() {}
    virtual void pretty_print() = 0;
    virtual Function * codegen() = 0;
};

#endif
