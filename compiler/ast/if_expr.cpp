#include "if_expr.h"

void IfExprAST::pretty_print() {
  std::cout << "if (";
  Condition->pretty_print();
  std::cout << ") {\n";
  for (auto &a : IfTrue) {
    a->pretty_print();
    std::cout << ";\n";
  }
  std::cout << "} else {";
  for (auto &a : IfFalse) {
    a->pretty_print();
    std::cout << ";\n";
  }
  std::cout << "}";
}

