#include <iostream>
#include "arg.h"
#include "expr.h"
#include "top_level_definition.h"

class FunctionDefinitionAST : public TopLevelDefinitionAST {
  std::string Name;
  std::string ReturnType;
  std::vector<std::unique_ptr<ArgAST>> Args;
  std::vector<std::unique_ptr<ExprAST>> Body;

  public:
  FunctionDefinitionAST(const std::string &Name,
      const std::string &ReturnType,
      std::vector<std::unique_ptr<ArgAST>> Args,
      std::vector<std::unique_ptr<ExprAST>> Body)
    : Name(Name), ReturnType(ReturnType), Args(std::move(Args)), Body(std::move(Body)) {}

  void pretty_print();
  Function * codegen();
};
