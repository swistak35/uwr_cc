#include "extern_definition.h"

void ExternDefinitionAST::pretty_print() {
  std::cout << "extern " << ReturnType << " " << Name << "(";
  for (auto &a : Args) {
    a->pretty_print();
    std::cout << ", ";
  }
  std::cout << ")\n";
}
