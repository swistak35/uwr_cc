#include <iostream>
#include "expr.h"

class ReturnExprAST : public ExprAST {
  std::unique_ptr<ExprAST> Result;

  public:
  ReturnExprAST(std::unique_ptr<ExprAST> Result) : Result(std::move(Result)) {}

  void pretty_print();
  Value * codegen();
};
