#include <iostream>
#include "expr.h"

class IfExprAST : public ExprAST {
  std::unique_ptr<ExprAST> Condition;
  std::vector<std::unique_ptr<ExprAST>> IfTrue;
  std::vector<std::unique_ptr<ExprAST>> IfFalse;

  public:
  IfExprAST(std::unique_ptr<ExprAST> Condition,
      std::vector<std::unique_ptr<ExprAST>> IfTrue,
      std::vector<std::unique_ptr<ExprAST>> IfFalse)
    : Condition(std::move(Condition)), IfTrue(std::move(IfTrue)), IfFalse(std::move(IfFalse)) {}

  void pretty_print();
  Value * codegen();
};
