#include <iostream>
#include "expr.h"

class IntExprAST : public ExprAST {
  int Val;

  public:
    IntExprAST(int Val) : Val(Val) {}

    int getVal() { return this->Val; }

    void pretty_print();
    Value * codegen();
};
