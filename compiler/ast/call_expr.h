#include <iostream>
#include "expr.h"

class CallExprAST : public ExprAST {
  std::string Callee;
  std::vector<std::unique_ptr<ExprAST>> Args;

  public:
  CallExprAST(const std::string &Callee,
      std::vector<std::unique_ptr<ExprAST>> Args)
    : Callee(Callee), Args(std::move(Args)) {}

  std::string getCallee() { return Callee; }
  std::vector<std::unique_ptr<ExprAST>> const& getArgs() {
    return Args;
  }

  void pretty_print();
  Value * codegen();
};
